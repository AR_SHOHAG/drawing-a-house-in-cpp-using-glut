#include <windows.h>
#include <GL\glut.h>

void myInit (void)

{

     glClearColor(0.0,0.0,0.0,0.0); // sets background color to white

     // sets a point to be 4x4 pixels

     glMatrixMode(GL_PROJECTION);

     glLoadIdentity();

     gluOrtho2D(0.0, 500.0, 0.0, 400.0); // the display area in world coordinates.

}


void myDisplay(void)

{

    glClear(GL_COLOR_BUFFER_BIT); // clears the screen
    glColor3f(0.0f,1.0f,0.1f) ;// sets the drawing color

     glPointSize(1);
     glLineWidth(1);

     glBegin(GL_POLYGON);
          glClear(GL_COLOR_BUFFER_BIT); // clears the screen
          glColor3f(0.0f,1.0f,0.0f) ;

          glVertex2i (10, 10);
          glVertex2i (480, 10);

          glVertex2i (10, 10);
          glVertex2i (10, 200);

          glVertex2i (10, 200);
          glVertex2i (480, 200);

          glVertex2i (480, 10);
          glVertex2i (480, 200);


     glEnd();

     glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(1.0f,1.0f,1.0f) ;

        glVertex2i (10, 200);
        glVertex2i (480, 200);

        glVertex2i (10, 200);
        glVertex2i (10, 350);

        glVertex2i (10, 350);
        glVertex2i (480, 350);

        glVertex2i (480, 200);
        glVertex2i (480, 350);

     glEnd();

        glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(1.0f,1.0f,1.0f) ;

        glVertex2i (30, 95);
        glVertex2i (50, 115);

        glVertex2i (30, 95);
        glVertex2i (50, 75);

        glVertex2i (70, 115);
        glVertex2i (50, 115);

        glVertex2i (70, 115);
        glVertex2i (90, 95);

        glVertex2i (70, 75);
        glVertex2i (90, 95);

        glVertex2i (70, 75);
        glVertex2i (50, 75);


     glEnd();

          glEnd();

        glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(0.0f,0.0f,0.3f) ;

        glVertex2i (140, 80);
        glVertex2i (150, 80);

        glVertex2i (150, 80);
        glVertex2i (150, 180);

        glVertex2i (150, 180);
        glVertex2i (140, 180);

        glVertex2i (140, 180);
        glVertex2i (140, 80);



     glEnd();

     glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(0.2f,0.7f,0.5f) ;

        glVertex2i (110, 180);
        glVertex2i (180, 180);

        glVertex2i (110, 180);
        glVertex2i (110, 220);


        glVertex2i (180, 220);
        glVertex2i (145, 275);

        glVertex2i (110, 220);
        glVertex2i (145, 275);

        glVertex2i (180, 220);
        glVertex2i (180, 180);





     glEnd();

     glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(0.5f,0.2f,0.5f) ;

        glVertex2i (330, 190);
        glVertex2i (450, 190);

        glVertex2i (435, 260);
        glVertex2i (450, 190);

        glVertex2i (435, 260);
        glVertex2i (345, 260);

        glVertex2i (390, 190);
        glVertex2i (345, 260);





     glEnd();

     glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(0.2f,0.0f,0.5f) ;

        glVertex2i (340, 100);
        glVertex2i (440, 100);

        glVertex2i (440, 190);
        glVertex2i (440, 100);

        glVertex2i (440, 190);
        glVertex2i (340, 190);

        glVertex2i (340, 100);
        glVertex2i (340, 190);

     glEnd();

        glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(0.2f,0.0f,0.5f) ;

        glVertex2i (340, 100);
        glVertex2i (440, 100);

        glVertex2i (440, 190);
        glVertex2i (440, 100);

        glVertex2i (440, 190);
        glVertex2i (340, 190);

        glVertex2i (340, 100);
        glVertex2i (340, 190);

     glEnd();

        glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(1.0f,0.0f,0.0f) ;

        glVertex2i (350, 170);
        glVertex2i (370, 170);
        glVertex2i (350, 170);
        glVertex2i (350, 150);
        glVertex2i (370, 150);
        glVertex2i (350, 150);
        glVertex2i (370, 150);
        glVertex2i (370, 170);

     glEnd();

        glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(1.0f,0.0f,0.0f) ;
        glVertex2i (410, 170);
        glVertex2i (410, 150);
        glVertex2i (430, 150);
        glVertex2i (410, 150);
        glVertex2i (430, 150);
        glVertex2i (430, 170);
        glVertex2i (410, 170);
        glVertex2i (430, 170);


     glEnd();

        glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(1.0f,1.0f,1.0f) ;

        glVertex2i (378, 175);
        glVertex2i (402, 175);
        glVertex2i (402, 100);
        glVertex2i (402, 175);
        glVertex2i (402, 100);
        glVertex2i (378, 100);
        glVertex2i (378, 175);
        glVertex2i (378, 100);

     glEnd();

        glBegin(GL_POLYGON);
        glClear(GL_COLOR_BUFFER_BIT); // clears the screen
        glColor3f(0.8f,0.6f,0.1f) ;

        glVertex2i (402, 100);
        glVertex2i (378, 100);

        glVertex2i (370, 10);
        glVertex2i (378, 100);

        glVertex2i (370, 10);
        glVertex2i (410, 10);
        glVertex2i (402, 100);
        glVertex2i (410, 10);

     glEnd();

          glBegin(GL_LINES);

     glEnd();


     glFlush(); // sends all output to display;

}
int main (int argc, char **argv)
{

     glutInit (&argc, argv); // to initialize the toolkit;

     glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB); // sets the display mode

     glutInitWindowSize (640, 480); // sets the window size

     glutInitWindowPosition (10, 10); // sets the starting position for the window

     glutCreateWindow ("My first OpenGL program!"); // creates the window and sets the title

     glutDisplayFunc (myDisplay);
     myInit(); // additional initializations as necessary
     glutMainLoop(); // go into a loop until event occurs
     return 0;
}
